const express = require('express'),
    request = require('request'),
    bodyParser = require('body-parser'),
    app = express();

const myLimit = typeof (process.argv[2]) != 'undefined' ? process.argv[2] : '100kb';
console.log('Using limit: ', myLimit);
app.use(bodyParser.json({limit: myLimit}));

let gatewayUrl = process.env.WSO2_GATEWAY_URL;
const gatewayAuth = process.env.WSO2_GATEWAY_AUTH;
const wso2CorsOrigin = process.env.WSO2_CORS_ORIGIN;
const gatewayPath = "/api-gateway";

function corsHeaders(res) {
    // Set CORS headers: allow all origins, methods, and headers: you may want to lock this down in a production environment
    if (wso2CorsOrigin) {
        res.header("Access-Control-Allow-Origin", wso2CorsOrigin);
    } else {
        res.header("Access-Control-Allow-Origin", '*');
    }
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", '*');
}

function check(res) {
    if (!gatewayUrl) {
        res.send(500, {error: 'URL do gateway não definido, defina a URL do gateway na variável de ambiente GATEWAY_URL'});
        return false;
    }
    if (!gatewayAuth) {
        res.send(500, {error: 'Autenticação do gateway não definido, defina o Header de Autenticação do gateway na variável de ambiente WSO2_GATEWAY_AUTH'});
        return false;
    }
    return true;
}

function log(error, response, url) {
    if(error || !response.statusCode || response.statusCode !== 200) {
        console.log('Erro ao chamar a URL ' + url);
        if(error) {
            console.log(error);
        }
        if(response.body) {
            console.log(response.statusCode + ': ' + response.body);
        } else if(response.statusCode) {
            console.log(response.statusCode + ': Body não informado.');
        }
    }
}

function copyHeader(req) {
    const headers = {...req.headers};
    delete headers.host;
    return headers;
}

function auth(res) {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': gatewayAuth,
    };
    return request({
        url: gatewayUrl + '/token',
        method: 'POST',
        headers: headers,
        form: {'grant_type': 'client_credentials'}
    }, function (error, response) {
        log(error, response, "/token");
    }).pipe(res);
}

function proxy(req, res, url) {
    const options = {
        url: gatewayUrl + url,
        method: req.method,
        headers: copyHeader(req),
        json: req.body
    };
    request(options, function (error, response) {
        log(error, response, url);
    }).pipe(res);
}

app.all(gatewayPath + '/*', function (req, res) {

    corsHeaders(res);

    if (req.method === 'OPTIONS') {
        // CORS Preflight
        res.send();
    } else {
        if (!check(res)) {
            return;
        }
        const url = req.url.replace(gatewayPath, '');
        if (url === '/token') {
            auth(res);
            return;
        }
        proxy(req, res, url);
    }
});

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function () {
    console.log('Proxy server listening on port ' + app.get('port'));
});
